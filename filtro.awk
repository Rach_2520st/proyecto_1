BEGIN {
	print "digraph G {"
	print "rankdir = LR;"
	print ""
	print ""
	print ""
	print ""
}

{
	#se le asigna un valor a cada columna que vaya leyendo el AWK
	elemento = $3
	aminoacido = $4
	cadena = $5
	id_aa = $6
	id_elemento = $2

	secuencia = cadena "_" id_aa "_" aminoacido
	secuenciaf = cadena "_" aminoacido "_" id_elemento "_" elemento


	if (residuos[secuencia] != "x") {
		print secuencia " [ label = \"" aminoacido "\" ]" 
		residuos[secuencia] = "x"
	}

	union = cadena "_" aminoacido
	
	if (cadenas[cadena][secuencia] != "ok") {
		cadenas[cadena][secuencia] = "ok"
	}
	
	print secuenciaf "[label = \""elemento"\"]"
	print secuenciaf " -> " secuencia

}

END {
	print ""
	print ""
	print ""
	print ""

	for (cadena in cadenas) {
		aa_anterior = 0

		for (aminoacido in cadenas[cadena]) {
			if (aa_anterior == 0) {
				aa_anterior = aminoacido
				continue
			}

			print aminoacido " -> " aa_anterior " [label = \"Cadena " cadena "\"]"
			aa_anterior = aminoacido
		}
	}

	print "}"
}
