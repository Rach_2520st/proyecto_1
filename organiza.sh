#!/bin/bash

echo "ingresa el código de la proteína que quiere visualizar"
read codigo

codigo=${codigo^^} #cambiar a mayusculas

if ! [[ ${#codigo} == 4 || "$codigo" == [A-Z0-9] ]]; then
	echo "ingresa un código valido, este debe contener el codigo correspondiente a la proteina que quieres visualizar"
	exit 1 #cierra la aplicación 
fi

if ! grep "\"$codigo\"" bd-pdb.txt | grep "Protein"; then
	echo "El código ingresado no es una proteína o no se encuentra en la base de datos, intentelo nuevamente"
	exit 1 
fi

if  [[ ! -f $codigo.pdb ]]; then
	wget https://files.rcsb.org/download/$codigo.pdb
fi

# Filtración al archivo que contiene la informacion(solo ATOM)
grep ^ATOM $codigo.pdb > Atom_$codigo.txt

# Le entrega el archivo pdb con todos los archivos filtrados a ATOM
# la siguiente linea hace que el programa en ocasiones en las que las columnas se encuentren con errores, estas se ajusten a las caracteristicas "estandar"
#en este proyecto
awk '{ print substr($0, 0, 16) " " substr($0, 18, 61) }' Atom_$codigo.txt | awk -f filtro.awk > graph.dot
#el siguiente comando genera la imagen en formato svg porque en png causaba problemas.
dot -Tsvg -o $codigo.svg graph.dot
echo "para visualizar la proteína que descargaste ingresa al repositorio contenedor del programa y visualiza $codigo.svg"