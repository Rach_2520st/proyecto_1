Visualiza y organiza aminoácidos

Este programa se caracteriza por la visualización de diferente proteínas que son almacenadas en la base de datos pdb, permite el ordenamiento de los aminoácidos según su cadena y orden en el que fueron visualizadas al ser estudiados, todo esto guíado por el archivo que entrega la base de datos de pdb.

Obtención del programa:

Para la obtención de este programa se debe clonar el actual repositorio, luego ingresar a la carpeta en la que se aloja el programa mediante la consola y escribir "bash organiza.sh" para que el programa se ejecute.

Prerrequisitos:

Encontrarse en un entorno linux
Tener instalada la librería graphviz

Objetivos:

Permitir visualizar la conformación de diferentes proteínas alojadas en la base de datos de pdb

Sobre el código:

Este programa contiene el código principal, la primera función que este cumple es revisar si lo que es ingresado es un código posible para la base de datos en la que se está trabajando, luego de analiza la base de datos y revisa si el código ingresado es una proteína o no lo es, en caso de que el código sea una proteína la descarga y continúa con el proceso, en el caso que este código no sea proteína envía el mensaje de error y se cierra el programa.
Cuando el programa revisa que efectivamente es una proteína y luego la descarga filtra las cadenas de texto que comienzan con ATOM del archivo pdb y los aloja en un arcivo .txt para luego pasarselo a AWK, en AWK se le asignan valores a cada columna y de este modo se va organizando el archivo.dot, esto se realiza mediante condicionales que van haciendo que se cree cierto orden para la creación del archivo dot que permite la generación de la imagen en formato svg que muestra el orden de los aminoácidos y sus elementos en la proteína, para tratar las excepciones se hace un filtro AWK antes de generar la imagen para que esta se pueda generar sin problemas.

Errores del programa:

Al generar el archivo dot, no queda muy ordenado, por lo tanto no es ordenado pero sí funcional. El formato de la imagen tuvo que ser cambiado de png a svg porque cuando la proteína era de un volumen mayor la imagen no se podía generar, debido al tener el formato svg, se tiene que tener programas especializados para visualizarlo o a través del navegador.

Autor:

Rachell Scarlett Aravena Martínez 
